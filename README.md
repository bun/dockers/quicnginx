# Ubuntu image to test QUIC in GNS-3

## Description

This docker image will contain the latest NGINX software supporting QUIC and HTTP/3.

### Additional packages added
The packages added to the base image are visible in the ```Dockerfile```.

## Build locally to test 

To test if the image can be build without error, test locally on your machine with the command:
```
docker build -t quicnginx .
```

## Force rebuild

If we change the content of the ```Dockerfile``` we have to add a new Tag to this repo. Then the CI/CD Pipeline will be executed.
Go to the "Repository --> Code --> Tag" menu to add the latest version number.

## How to use this image

- To download and use this image, just type:
  ```bash
  docker pull registry.forge.hefr.ch/bun/dockers/quicnginx
  ```

- To run this image and mount a local volume:
  ```bash
  docker run -it -p4443:4443 -p443:443 -p80:80 -v ~/mytmp:/mytmp registry.forge.hefr.ch/bun/dockers/quicnginx bash
  ```
  This will map the directory ```~/mytmp``` to the directory ```mytmp``` inside the container.

## More informations
See Gitlab Docs:
* https://docs.gitlab.com/ee/user/packages/container_registry/
* https://www.bing.com/videos/riverview/relatedvideo?q=quic%20lab%20with%20chrome%20and%20nginx&mid=B5952F916CF6846E5352B5952F916CF6846E5352&ajaxhist=0


<hr>
(c) F. Buntschu 18.11.2024