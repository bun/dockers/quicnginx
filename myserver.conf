server {
    listen 80;
    listen 443 quic reuseport;
    listen 443 ssl;
    http2 on;
    http3 on;
    server_name  localhost;

    ssl_certificate      /etc/ssl/certs/nginx-selfsigned.crt;
    ssl_certificate_key  /etc/ssl/private/nginx-selfsigned.key;

    # To enable address validation
    quic_retry on;
    # To enable 0-RTT
    ssl_early_data on;
    # To enable GSO (Generic Segmentation Offloading)
    quic_gso on;

    location / {
        root   /usr/local/nginx/html;
        index  index.html index.htm;
        add_header alt-svc 'h3=":443"; ma=1800, h3-29=":443"; ma=1800' always;
        add_header x-quic 'h3';
        add_header Cache-Control 'no-cache,no-store';
        # Used to display the HTTP version
        add_header X-Http-Protocol $server_protocol;
    }

    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   html;
    }

}