# Builds NGINX from the QUIC+HTTP/3 development branch
# - Based on the official NGINX docker image, including all modules built by default
# - OpenSSL replaced with LibreSSL to support QUIC's TLS requirements (statically linked)
#
# docker build --no-cache -t nginx:quic .
# docker run -d -p 443:443 -p 443:443/udp nginx:quic
#
# Note that a suitable configuration file and TLS certificates are required for testing!
# See <https://quic.nginx.org/readme.html> for more info

FROM nginx AS build

WORKDIR /src
RUN apt-get update && apt-get install -y git gcc make autoconf libtool perl iproute2 vim nano iputils-ping
RUN git clone -b v3.9.2 https://github.com/libressl-portable/portable.git libressl && \
    cd libressl && \
    ./autogen.sh && \
    ./configure && \
    make check && \
    make install

RUN apt-get install -y libperl-dev libpcre3-dev zlib1g-dev libxslt1-dev libgd-ocaml-dev libgeoip-dev wget
RUN cd /src && \
    wget http://nginx.org/download/nginx-1.27.2.tar.gz && \
    tar -zxvf nginx-1.27.2.tar.gz
RUN cd nginx-1.27.2 && \
    ./configure \
      --with-debug \
      --with-http_v3_module \
      --with-cc-opt="-I../libressl/build/include" \
      --with-ld-opt="-L../libressl/build/lib" \
      --sbin-path="/usr/local/nginx/nginx" \
      --conf-path="/usr/local/nginx/nginx.conf" \
      --pid-path="/usr/local/nginx/nginx.pid" && \
    make && \
    make install

RUN cp /usr/local/nginx/nginx.conf /usr/local/nginx/nginx.conf.ORIG
# Generating Self-signed certificate
RUN openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt -subj "/C=CH/ST=FR/L=Fribourg/O=Organization/OU=Unit/CN=isc.heia-fr.ch"

COPY nginx.conf /etc/nginx/
COPY myserver.conf /etc/nginx/conf.d/
RUN rm -rf /etc/nginx/conf.d/default.conf
COPY html/* /usr/local/nginx/html/

RUN set -x \
    && apt -y autoremove

EXPOSE 80 443 443/udp

ENTRYPOINT ["/bin/bash", "-c"]
CMD ["nginx -g 'daemon off;'"]